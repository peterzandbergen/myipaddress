# Demo met config maps en deployment

Monitor de applicatie
```
watch -n 1 curl -s http://localhost/myipaddress
```

Update de applicatie
- pas naam aan in config.env
- run update command

```
kustomize build . | kubectl apply -f -
```

