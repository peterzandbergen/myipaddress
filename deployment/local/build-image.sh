#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

echo Image: $IMAGE

DOCKER_CMD=podman
# DOCKER_CMD=docker

cd $(dirname $0)/../..

echo Building image from dir $(pwd)
# Build the image
echo $DOCKER_CMD build --tag ${IMAGE} --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
$DOCKER_CMD build --tag ${IMAGE} --tag docker.io/peterzandbergen/${IMAGE} --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
