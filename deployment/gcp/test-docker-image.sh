#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

# Remove local image
$DOCKER_CMD image remove $IMAGE

# Pull the image and run the container
CONTAINER_ID=$($DOCKER_IMAGE run --rm -d -p 18080:8000 $IMAGE)

curl  \
    --header 'Accept: application/json' \
    --header "X-AuthToken: $APIKEY" \
    --data '{"max_proc": 0,"timeout": 60,"urls":["https://vragenlijst1.cbs.nl/AFBEDR18","https://vragenlijst2.cbs.nl/AFBEDR18","https://vragenlijst3.cbs.nl/AFBEDR18"]}' \
    "https://healthprober-hgpu2hhtya-ez.a.run.app/probe"

CURL_EXIT=$?

echo Killing container...
$DOCKER_CMD kill $CONTAINER_ID

echo
echo Curl exit code $?

