#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

echo Image: $IMAGE

cd $(dirname $0)/../..

echo Building image from dir $(pwd)

echo Using container tool $DOCKER_CMD
# Build the image
echo Building from directory $(pwd)

echo $DOCKER_CMD build $BUILD_FORMAT --tag ${IMAGE} --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
$DOCKER_CMD build $BUILD_FORMAT --tag ${IMAGE} --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
# echo $DOCKER_CMD build --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
# $DOCKER_CMD build --file docker/Dockerfile --build-arg GIT_COMMIT=$GIT_COMMIT .
# Push to the registry
echo $DOCKER_CMD push $PUSH_FORMAT ${IMAGE}
$DOCKER_CMD push $PUSH_FORMAT ${IMAGE}
