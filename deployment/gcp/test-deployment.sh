#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

# source /home/peza/Documents/healthprober/apikey

curl  \
    --header 'Accept: application/json' \
    --header "X-AuthToken: $APIKEY" \
    --data '{"max_proc": 0,"timeout": 60,"urls":["https://vragenlijst1.cbs.nl/AFBEDR18","https://vragenlijst2.cbs.nl/AFBEDR18","https://vragenlijst3.cbs.nl/AFBEDR18"]}' \
    "https://healthprober-hgpu2hhtya-ez.a.run.app/probe"

echo Curl exit code $?