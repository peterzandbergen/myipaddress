# Project settings
GCLOUD_CONFIG=myipaddress
PROJECT_ID=myipaddress-307809
REGION=europe-west4

IMAGE_NAME=myipaddress

DOCKER_CMD=podman

BUILD_FORMAT="--format oci"
PUSH_FORMAT="--format v2s2"

# Set the gcloud environment.
gcloud config configurations activate $GCLOUD_CONFIG
gcloud config set project $PROJECT_ID

#  Derive settings from the gcloud environment
PROJECT=$(gcloud config get-value project)
PROJECT_NUMBER="$(gcloud projects list --filter=$(gcloud config get-value project) --format='value(PROJECT_NUMBER)')"

# Set the gcloud environments
gcloud config set run/region ${REGION}

# Git info
GIT_COMMIT=$(git rev-parse --short HEAD)

# Image settings
MEMORY=300Mi
TIMEOUT=300s
SERVICE_NAME=myipaddress
# IMAGE_REGISTRY_HOST=eu.gcr.io
REPO_NAME=europe-west4-docker.pkg.dev/myipaddress-307809/test-repo

# for testing
# IMAGE_TAG=test-001
# IMAGE=${IMAGE_REGISTRY_HOST}/${PROJECT}/${IMAGE_NAME}:${GIT_COMMIT}
IMAGE=${REPO_NAME}/${IMAGE_NAME}:${GIT_COMMIT}
echo Image: $IMAGE

# Change for the project.
SERVICE_ACCOUNT_ID=sa-myipaddress
SERVICE_ACCOUNT="${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com"




